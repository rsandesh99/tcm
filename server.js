const express = require('express');
const bodyParser = require('body-parser');
var mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const cors = require('cors');
//const PORT = process.env.PORT;
var users = require('./models/userModel')
const signin = require('./controllers/signin');
const register = require('./controllers/register');
const profile = require('./controllers/profile');
const image = require('./controllers/image');





const dbpath ="mongodb://sandesh99:alexandria123@ds017553.mlab.com:17553/tcm";
db = mongoose.connect(dbpath);

const app = express();

var routes = require('./routes/tcmroutes')
routes(app);

app.use(bodyParser.json());
app.use(cors());


app.get('/', (req, res)=> res.send('It is working'));
app.post('/signin', (req, res) => signin.handleSignIn(req, res, db, bcrypt));
app.post('/register',(req, res)=> register.handleRegister(req, res, db, bcrypt));
app.get('/profile/:id', (req, res) => profile.handleProfileGet(req, res, db));
app.put('/image', (req, res) => image.handleImage(req, res, db));
app.post('/imageurl', (req, res)=> image.handleApiCall(req, res));

app.listen(process.env.PORT || 3000, () => {
    console.log(`Server started on port ${process.env.PORT}`);
});